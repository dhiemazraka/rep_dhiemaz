package com.example.demo.basic;

public class Bilprima
{

    public Integer[] tampil(Integer range)
    {
//        untuk menampung ada berapa bilangan prima
        Integer tampung = 0;
        for (int a=1; a<=range; a++)
        {
            int cari = 0;
            for (int b=1; b <= a; b++   )
            {
                if (a % b == 0)
                {
                    cari ++;
                }
            }

            if (cari == 2)
            {
                tampung += 1;
            }

        }
//        batas tampung

//        untuk mencari bilangan prima
        Integer hasil[] = new Integer[tampung];
        int prima = 0;
        for (int a=1;  a<=range; a++)
        {
            int cari = 0;
            for (int b=1; b<=a; b++)
            {
                if (a % b == 0)
                {
                    cari++;
                }
            }
            if (cari == 2)
            {
                hasil[prima]=a;
                prima += 1;
            }
        }
        return hasil;
    }
}
